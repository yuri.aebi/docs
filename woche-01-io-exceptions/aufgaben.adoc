= Aufgaben
:stem: latexmath
:icons: font
:hide-uri-scheme:

== Obligatorische Aufgabe

Für diese und nächste Woche gibt es eine _obligatorische Aufgabe_: <<ACO-Viewer>>. Der Abgabetermin ist am *05.03.2023, um 23:59*. Die Abgabe erfolgt durch einen _Merge Request_ auf den `submission`-Branch in Ihrem persönlichen GitLab-Repository auf https://gitlab.fhnw.ch. Details zu diesem Vorgehen werden in der zweiten Semesterwoche besprochen, eine Anleitung finden Sie xref:abgabe-oa.adoc[hier].


:sectnums:
== Code mit Exceptions verfolgen (Vorlesung)

Welche Ausgabe produziert dieses Codestück:

[source,java]
----
try {
    ArrayList<String> list = /* HIER */;
    System.out.println("Erstes Element: " + list.get(0));
} catch (NullPointerException e) {
    System.out.println("NPE gefangen!");
}
System.out.println("Alles gut.");
----

wenn man bei `/* HIER */` folgendes einsetzt?

[loweralpha]
. `new ArrayList<>(List.of(7, 2, 5))`
. `new ArrayList<>()`
. `null`


== `try`–`catch` (Vorlesung)

In Ihrer Vorlage finden Sie das Programm `TryCatch`, welches eine Datei in einen anderen Ordner kopiert. Falls eine Exception auftritt, bekommt der User bisher einen hässlichen Stack Trace zu sehen.

Ändern Sie das Programm mittels `try`–`catch` so ab, dass es I/O-Exceptions fängt und eine benutzerfreundliche Nachricht ausgibt. Unterscheiden Sie zwischen dem Fall, dass eine Datei nicht gefunden wurde (`NoSuchFileException`) und anderen Fehlern beim Kopieren (`IOException`).

== PDF-Checker

Schreiben Sie ein Programm `PdfChecker`, welches den User nach einem Pfad zu einer Datei fragt und bestimmt, ob diese Datei eine PDF-(kompatible) Datei ist.

Identifizieren Sie PDF-Dateien…

[loweralpha]
. ...anhand der Dateiendung .pdf
. ...anhand der ersten 5 Bytes der Datei (der sogenannten _Signatur_). PDF-kompatible Dateien beginnen mit `0x25` `0x50` `0x44` `0x46` `0x2D` (als ASCII-String: `%PDF-`). Das beinhaltet z. B. auch Adobe-Illustrator-Dateien, welche üblicherweise die Dateiendung .ai verwenden.
+
Siehe auch: https://en.wikipedia.org/wiki/List_of_file_signatures[List of file signatures (Wikipedia)].

Achten Sie darauf, dass Sie alle Ressourcen korrekt schliessen, und implementieren Sie User-freundliches Exception-Handling.


== ACO-Viewer

IMPORTANT: Das ist eine obligatorische Aufgabe und _muss eigenhändig und alleine gelöst werden._ Der Abgabetermin ist am *05.03.2023, um 23:59*.

In dieser Aufgabe implementieren Sie eine App, welche eine Farbpalette aus einer .aco-Datei liest und mittels der aus OOP1 bekannten `Window`-Klasse anzeigt. Das .aco-Dateiformat ist ein Binärformat, das unter anderem von Adobe Photoshop verwendet wird. (Das Kürzel ACO steht für _Adobe Color_).

[.text-center]
image:res/aco-viewer.png[width=600]

Ihre Aufgabe besteht darin, ohne Code-Vorlage ein funktionsfähiges Programm zu schreiben. Zudem sollen Sie sich an eine Grobstruktur halten, welche folgende Klassen umfasst:

`Swatch`:: Ein Objekt dieser Klasse entspricht einem Eintrag in der Palette und beinhaltet die Farbe selbst (als Objekt der Klasse `Color` aus dem Package `gui`) und den Namen der Farbe, der aber auch `null` sein kann (falls in der .aco-Datei keine Namen hinterlegt sind, siehe unten).
`AcoSwatchesReader`:: Enthält eine statische Methode `read(Path acoFile)`, welche die Informationen in der gegebenen .aco-Datei einliest und als Liste von `Swatch`-Objekten zurückgibt.
`AcoViewer`:: Enthält die `main`-Methode, welche als Argument einen Pfad zu einer .aco-Datei bekommt, diese mittels `AcoSwatchesReader` einliest und die Palette in einem `Window` anzeigt. Die Anzeige soll ungefähr wie im Screenshot oben aussehen, aber in den Details (Abstände, Schriftgrösse, Hintergrund- und Textfarben) sind Sie frei.
+
Falls Sie nicht im letzten Semester OOP1 besucht haben, finden Sie https://gitlab.fhnw.ch/2022hs-oop1/docs/-/blob/main/woche-07/GUI-Programmierung.pdf[hier] die wichtigsten Informationen zu `Window`-Klasse.

Erstellen Sie diese Klassen in einem Package `acoviewer` im Projekt «aufgaben-01» in Ihrem persönlichen Git-Repository.


=== Das .aco-Format

Das .aco-Format wurde von Adobe mehrmals erweitert und bietet viele Möglichkeiten wie z. B. unterschiedliche Farbräume und -tiefen und Gruppierung von Swatches. Für diese Aufgabe beschränken wir uns auf die ersten zwei Versionen der Formats (V1 & V2) und auf Farben im RGB-Raum, mit 8-Bit-Farbtiefe.

Version V1 kann nur Farben ohne Namen speichern. Sie besteht aus lauter _unsigned_ 16-Bit-_Words_ (d. h. 2-Byte-Werten) und beinhaltet einen 2-Word-Header und eine Folge von 5-Word-Blöcken. Eine V1-Datei mit fünf Farben könnte z. B. so aussehen:

[.text-center]
image:res/aco-v1.svg[width=800]

Die erste Farbe in dieser Palette ist `#6A0E15`, die zweite `#242757`, die dritte `#FF7E14`, usw. Da es sich hier um 8-Bit-Farbwerte handelt, würde eigentlich 1 Byte pro Farbwert reichen; das Format verwendet aber trotzdem immer 2 Bytes, wobei einfach beide auf den gleichen Wert gesetzt sind.

V2 enthält zusätzlich zu den Farbwerten auch die Namen der Swatches. Das Format wurde rückwärtskompatibel entworfen, weshalb jede V2-Datei mit dem V1-Header _und den V1-Farbblöcken_ beginnt. Danach folgt erst der V2-Header und eine Folge von Farbblöcken mit Namen. Da die Namen unterschiedlich lang sein können, sind auch die Längen der V2-Farbblöcke variabel. Eine V2-Datei mit drei Farben könnte z. B. so aussehen:

[.text-center]
image:res/aco-v2.svg[width=800]

Die erste Farbe in dieser Palette ist `#B30006` namens `Red`, die zweite `#0057A6` namens `Blue` und die dritte `#FF8172` namens `Coral`. Jedes Zeichen der Namen belegt ein ganzes 2-Byte-Word; im Beispiel oben würde zwar auch 1 Byte reichen, aber für gewisse Zeichen sind tatsächlich zwei Bytes erforderlich. Jeder Name wird zudem mit einem `0`-Word abgeschlossen (was eigentlich nicht nötig wäre, da die Länge des Namens ebenfalls gespeichert wird).


=== Anforderungen

Damit die Aufgabe als bestanden gilt, muss Ihre Abgabe folgende Detailanforderungen erfüllen:

. *V1 & V2:* Ihr Implementation soll .aco-Dateien sowohl im V1- als auch im V2-Format lesen können. Im Fall von V1-Dateien sollen einfach die Hex-Codes der Farben angezeigt werden.

. *Schliessen von Ressourcen:* Falls Sie einen `InputStream` verwenden (was empfohlen ist), sollen Sie sicherstellen, dass dieser nach der Verwendung garantiert geschlossen wird.

. *Exception Handling:* Falls beim Lesen oder Parsen der .aco-Datei Fehler auftreten, soll der `AcoSwatchesReader` eine `IOException` werfen. Der Code in `AcoViewer` soll diese fangen und dem User eine sinnvolle Fehlermeldung anzeigen.

. *Darstellung der Swatches:* Sie können grundsätzlich frei wählen, wie Ihr `AcoViewer` die Swatches darstellt. Als Mindestanforderung muss die Ansicht aber mehrere Zeilen und Spalten unterstützen, um grosse Paletten sinnvoll darstellen zu können. Die Implementation im Screenshot oben stellt die Swatches von oben nach unten dar, fügt aber weitere Spalten hinzu, falls mehr als 15 Swatches vorhanden sind.

. *Verwendung von `args`:* Ihr Programm soll den Pfad zur .aco-Datei als _Programmargument_ entgegen nehmen, d. h. diesen aus dem `args`-Array lesen, das der `main`-Methode übergeben wird. Falls das Programm ohne Argument aufgerufen wird, soll dem User eine sinnvolle Fehlermeldung angezeigt werden.
+
In IntelliJ können Sie die Programmargumente in der _Run Configuration_ definieren. Beachten Sie auch das _working directory_; dieses wird als Basis für relative Pfade verwendet.
+
[.text-center]
image::res/program-args.png[width=500]


=== Hinweise & Tipps

. Da das gesamte .aco-Format auf 2-Byte-Words basiert, empfiehlt es sich, in `AcoSwatchesReader` eine Hilfsmethode zu schreiben, welche zwei Bytes aus der Datei liest und diese in die entsprechende Zahl umwandelt. Denken Sie an die bitweisen Operationen!

. Für den Umgang mit den 2-Byte-Words scheint der Java-Datentyp `short` prädestiniert zu sein, welcher ja genau zwei Bytes umfasst. Allerdings ist `short` ja _signed_, die Werte in einer .aco-Datei hingegen alle _unsigned_. Dasselbe gilt, wenn Sie mit einzelnen Bytes arbeiten, z. B. für die RGB-Werte; dort denkt man auch zuerst an den Datentyp `byte`, aber dieser ist ebenfalls _signed_. Aus diesem Grund ist es einfacher, überall den Typ `int` zu verwenden, welcher genügend gross ist für _unsigned_ Byte- oder 2-Byte-Werte.

. Für die Konvertierung der Farbnamen vom Binärformat zu Java-Strings müsste man sich eigentlich mit der _Textcodierung_ des .aco-Formats beschäftigen. Dieses Thema kommt aber erst später; für diese Aufgabe können Sie eine einfache Implementation verwenden, welche jedes 2-Byte-Word einzeln zu einem `char`-Wert castet und an einen String anhängt.

. Um Sie beim Implementieren von `AcoSwatchesReader` zu unterstützen, stehen in der Datei link:AcoSwatchesReaderTest.java[AcoSwatchesReaderTest.java] einige Unit-Tests zur Verfügung. Sie können diese Datei in Ihr Projekt kopieren und ausführen.
+
Die Tests nehmen an, dass eine Methode `read(InputStream in)` vorhanden ist, und verwendet einen `ByteArrayInputStream`, um die Daten direkt aus einem `byte`-Array zu lesen; das ist einfacher als mit Dateien und Pfaden zu arbeiten. Für Ihre `AcoSwatchesReader`-Klasse empfiehlt es sich, in der Methode `read(Path acoFile)` einfach einen `InputStream` zu erstellen und die Hauptarbeit der anderen Methode zu überlassen.

. Im Order «swatches» finden Sie einige .aco-Dateien mit unterschiedlich vielen Swatches, welche Sie zum Testen der Darstellung verwenden können. Einige der Dateien sind im V1-, andere im V2-Format. Stellen Sie vor der Abgabe sicher, dass Ihr Programm sämtliche dieser .aco-Dateien lesen kann und sinnvoll darstellt.
