= Aufgaben
:stem: latexmath
:icons: font
:hide-uri-scheme:


:sectnums:
== Einstieg in JWM & Skija (Vorlesung)

NOTE: Diese Aufgabe wird gemeinsam in der Vorlesung gelöst. Wenn Sie nicht in der Vorlesung waren, verwenden Sie die xref:jwm-skija.adoc[Ressourcen zu JWM & Skija] um die Aufgabe selbstständig zu lösen.

Erweitern Sie das Programm `WordQuiz` in Ihrer Vorlage so, dass ein Fenster geöffnet wird, in dem der User die verwürfelten Buchstaben eines Worts anklicken und dadurch in die korrekte Reihenfolge bringen soll:

[.text-center]
image:res/word-quiz-1.gif[width=500]


== `Comparator` implementieren (Vorlesung)

[loweralpha]
. Erweitern Sie das Programm `Hotels` in Ihrer Vorlage so, dass die Liste der Hotels _nach Sterne sortiert_ ausgegeben wird. Erstellen Sie dazu eine Klasse, die `Comparator` implementiert, und rufen Sie in der `main`-Methode `Collections.sort()` auf.

. Ändern Sie den `Comparator` so ab, dass als zweites Kriterium noch nach Name sortiert wird:
+
----
***** Les Trois Rois
**** Hyperion
**** Radisson Blu
*** Hotel Rochat
*** Motel One
** Rheinfelderhof
* ibis Budget
----


== Lambdas & Event-Handling (Vorlesung)

Ändern Sie Ihre `WordQuiz`-Implementation (siehe Aufgabe <<Einstieg in JWM & Skija (Vorlesung),Einstieg in JWM & Skija>>) so, dass statt den Extra-Klassen, welche `Runnable` und `Consumer` implementieren, zwei Lambda-Ausdrücke verwendet werden.


== Funktionsinterfaces (Vorlesung)

Entscheiden Sie, ob es sich um Funktionsinterfaces handelt:

[loweralpha]
. {empty}
+
[source,java]
----
public interface StringTester {
    boolean test(String s);
}
----
. {empty}
+
[source,java]
----
public interface Supplier<T> {
    T get();
}
----
. {empty}
+
[source,java]
----
public interface IntPredicate {
    boolean test(int val);
    default IntPredicate negate() {
        return val -> !test(val);
    }
}
----
. {empty}
+
[source,java]
----
public interface Function<T, R> {
    R apply(T t);
    static <T> Function<T, T> identity() {
        return t -> t;
    }
}
----
. {empty}
+
[source,java]
----
public interface UnaryOperator<T> extends Function<T, T> {}
----
. {empty}
+
[source,java]
----
public interface Cache<K, V> extends Function<K, V> {
    V get(K key);
}
----


== Methodenreferenzen (Vorlesung)

Im AD finden Sie ein Kapitel aus _https://horstmann.com/javaimpatient/index2.html[Core Java SE 9 for the Impatient]_ von Cay Horstmann. Lesen Sie Abschnitt 3.5 «Method and Constructor References» (Seiten 116 – 119).

[loweralpha]
. Studieren Sie dann das Programm `Methodenreferenzen` in Ihrer Vorlage. Finden Sie heraus, welche Ausgabe es produziert (natürlich ohne es auszuführen). Ziehen Sie ggf. die Java-Dokumentation zur Rate, indem Sie in der IDE z. B. mit der Maus über den Namen von Klassen oder Methoden fahren.

. Ersetzen Sie alle Methoden-/Konstruktorenreferenzen mit gleichwertigen Lambda-Ausdrücken.


== `WordQuiz` erweitern

Erweitern Sie das `WordQuiz`-Programm, dass es ungefähr so aussieht:

[.text-center]
image:res/word-quiz-2.gif[width=600]

[loweralpha]
. Die Buchstaben sehen aus wie Scrabble-Steine, mit gerundeten Ecken.
. Ein ausgewählter Stein wird rot umrandet.
. Wenn die Maus über einen Stein fährt, wie der Stein leicht hervorgehoben.
. Wenn das Wort gefunden wurde, werden alle Steine grün.

Ziehen Sie dafür die xref:jwm-skija.adoc[Ressourcen zu JWM & Skija] zur Rate, insbesondere die Beschreibungen der Klassen `Canvas`, `RRect` & `EventMouseMove`.